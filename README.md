gRijndael
=========

This is *Toy project* to port what was written in python in the 
[Rijndael](https://gitlab.com/srgblnch/Rijndael) repo to Rust.

Testing
-------

Currently developing the polynomials behind Rijndael. So, basically the tests 
in `tests/rijndael.rs` are not ready yet.

To execute the tests in the current development:

```
cargo test --test binary_polynomials
```

To see the `println!`s:

```
cargo test --test binary_polynomials -- --nocapture --test-threads=1
```

To launch one specific test in the files, use the name of the test. For 
example, to only test the cyclic shift:

```
cargo test --test binary_polynomials -- --nocapture --test test_cyclic_shift
```
