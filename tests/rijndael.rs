use grijndael::Grijndael;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn aes() {
        let result = Grijndael::new(10, 4, 4, 8, 4);
        assert_eq!(result.n_rounds(), 10);
        assert_eq!(result.block_size(), 128);
        assert_eq!(result.key_size(), 128);
        // TODO: test vectors to cipher and decipher
    }

    #[test]
    fn rijndael160() {
        let result = Grijndael::new(11, 4, 4, 8, 5);
        assert_eq!(result.n_rounds(), 11);
        assert_eq!(result.block_size(), 128);
        assert_eq!(result.key_size(), 160);
        // TODO: test vectors to cipher and decipher
    }

    #[test]
    fn aes192() {
        let result = Grijndael::new(12, 4, 4, 8, 6);
        assert_eq!(result.n_rounds(), 12);
        assert_eq!(result.block_size(), 128);
        assert_eq!(result.key_size(), 192);
        // TODO: test vectors to cipher and decipher
    }

    #[test]
    fn rijndael224() {
        let result = Grijndael::new(13, 4, 4, 8, 7);
        assert_eq!(result.n_rounds(), 13);
        assert_eq!(result.block_size(), 128);
        assert_eq!(result.key_size(), 224);
        // TODO: test vectors to cipher and decipher
    }

    #[test]
    fn aes256() {
        let result = Grijndael::new(14, 4, 4, 8, 8);
        assert_eq!(result.n_rounds(), 14);
        assert_eq!(result.block_size(), 128);
        assert_eq!(result.key_size(), 256);
        // TODO: test vectors to cipher and decipher
    }
    // TODO: extend to other non-standard sizes
}