# Polynomials using sagemath

## Launch your sagemath instance

We can use [sagemath/docker-images](https://github.com/sagemath/docker-images):

```commandline
docker run --name sagemath --rm -p 8888:8888 sagemath/sagemath-jupyter 
```

Binary polynomials:

```
>>> P.<z> = PolynomialRing(GF(2))
>>> z^9
 z^9
```

Those binary polynomials quotient an irreducible polynomial, so it build a field:

```
>>> Q.<z> = P.quotient(z^8+z^4+z^3+z+1)
>>> z^9
 z^5 + z^4 + z^2 + z
```

Create random elements and add them:

```
>>> a = Q.random_element(); a_str = str(a).replace(' ','')
>>> b = Q.random_element(); b_str = str(b).replace(' ','')
>>> r = a+b; r_str = str(r).replace(' ','')
>>> print("(\"%s\", \"%s\", \"%s\")" % (a_str, b_str, r_str))
```

Then you have a valid string to be placed in the tests.