#[path = "../src/polynomials/binary_polynomials.rs"] mod binary_polynomials;

use std::panic;
use binary_polynomials::{BinaryExtensionModuloConstructor,BinaryExtensionModulo};

// use binary_polynomials::degree;


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_good_polynomials() {
        let field = BinaryExtensionModuloConstructor::new("z^15+z+1", 'z');
        let polynomials_vector: Vec<(&str, u16, u8)> = vec![
            ("0", 0, 0), ("1", 1, 0), ("z^0", 1, 0), ("z^1", 2, 1), ("z^1+1", 3, 1),
            ("z^3+z^2+z^1+1", 15, 3), ("z^4+z^3+z^2+z^1+1", 31, 4),
            ("z^5+z^4+z^3+z^2+z^1+1", 63, 5), ("z^6+z^5+z^4+z^3+z^2+z^1+1",  127, 6),
            ("z^7+z^6+z^5+z^4+z^3+z^2+z^1+1", 255, 7), ("z^8", 256, 8),
            ("z^8+z^4+z^3+z+1", 283, 8), ("z^8+z^10", 1280, 10), ("z^8+z^12", 4352, 12),
            // start reducing modulo, this tests division()
            ("z^15", 32768, 15), ("z^15+z^3+z^2+z^1+1", 12, 3), ("z^15+z^4+z^3+z^2+z^1+1", 28, 4),
            ("z^15+z^5+z^4+z^3+z^2+z^1+1", 60, 5), ("z^15+z^6+z^5+z^4+z^3+z^2+z^1+1",  124, 6),
            ("z^15+z^7+z^6+z^5+z^4+z^3+z^2+z^1+1", 252, 7), ("z^15+z^8", 259, 8),
            ("z^15+z^8+z^4+z^3+z+1", 280, 8), ("z^15+z^8+z^10", 1283, 10),
            ("z^15+z^8+z^12", 4355, 12),
        ];
        for pair in polynomials_vector {
            let (polynomial_str, value, degree): (&str, u16, u8) = pair;
            let polynomial = field.from_string(polynomial_str);
            // assert_eq!(polynomial.coefficients(), value);
            // assert_eq!(polynomial.degree(), degree);
            // println!("{:>35} => {:45} 0x{:<5X} {:2} == {:<2}", polynomial_str, polynomial.to_string(),
            //          polynomial.coefficients(), polynomial.degree(), degree);
            // I cannot compare all the rebuild strings because some have different
            // order or simplification
        }
    }

    #[test]
    fn test_invalid_polynomials() {
        let field = BinaryExtensionModuloConstructor::new("z^8+z^4+z^3+z+1", 'z');
        let invalid_polynomials: Vec<&str> = vec![
            "x", "x^3",  // different variable name
            "z^16",  // too big
            "z^8+z^4*z^3+z+1",  // invalid "*" symbol
        ];
        for bad_sample in invalid_polynomials {
            let result = std::panic::catch_unwind(|| { field.from_string(bad_sample) });
            assert!(result.is_err());
        }
        // binary_extension_modulo("z^8+z^4+z^3+z+1", 'x');
        // binary_extension_modulo("z8+z*4+z^3+x+1", 'x');
        // // TODO: mode examples of typos
    }

    // TODO: test degree, hamming weight, string representation

    // test operations between polynomials (add, mul, div)

    #[test]
    fn test_addition(){
        let field = BinaryExtensionModuloConstructor::new("z^8+z^4+z^3+z+1", 'z');
        let polynomials_vector: Vec<(&str, &str, &str)> = vec![
            ("z^5+z^4+z^2+z", "z^6+z^5+z^3+z^2", "z^6+z^4+z^3+z"),
            ("z^5+z^4+1", "z^3+z+1", "z^5+z^4+z^3+z"),
            ("z^6+z^4+z^3+z+1", "z^7+z^4+z^3+z+1", "z^7+z^6"),
            ("z^7+z^6+z^5+z^3+z^2+z+1", "z^7+z^5+z^4+z^3+z^2+1", "z^6+z^4+z"),
            ("z^7+z^5+z^3", "z^7+z^5+z^4+z^3+z", "z^4+z"),
            ("z^7+z^6+z^5+z^3+z^2+z", "z^7+z^5+z^4+z^2+z", "z^6+z^4+z^3"),
            ("z^7+z^4+z^3+z+1", "z^7+z^6+z+1", "z^6+z^4+z^3"),
            ("z^7+z^4+z", "z^7+z^5+z^4+z^3+z^2+z", "z^5+z^3+z^2"),
        ];
        for pair in polynomials_vector {
            let (a_str, b_str, r_str): (&str, &str, &str) = pair;
            let a = field.from_string(a_str);
            let b = field.from_string(b_str);
            let r = field.from_string(r_str);
            // println!("{} == {}", (a+b).to_string(), r.to_string());
            assert!(a+b == r);
        }
    }

    #[test]
    fn test_multiplication(){
        let field = BinaryExtensionModuloConstructor::new("z^8+z^4+z^3+z+1", 'z');
        let polynomials_vector: Vec<(&str, &str, &str)> = vec![
            ("z^5+z^4+z^2+z", "z^6+z^5+z^3+z^2", "z^7+z^6+z^2+z"),
            ("z^7+z^6+z^5+z^2+1", "z^7+z^6+z^5+z^4+z^3+z", "z^6+z^5+z^4+z^3+z"),
            ("z^7+z^3+z^2", "z^7+z^5+z^4+1", "z^6+z^5+z^2"),
            ("z^7+z^6+z^3+z^2+z", "z^7+z^5+z^2+1", "z^7+z^6+z^5+z^2+1"),
            ("z^7+z^6+z^2", "z^7+z^5+z^3+1", "z^7+z^5+z^3+z^2"),
            ("z^7+z^6+z^5+z+1", "z^7+z^4+z^3+z", "z^7+z^6+z^4+1"),
            ("z^7+z^6+z^5+z^4+z^2+1", "z^7+z^4", "z^7+z^6+z^4+z^3+z+1"),
            ("z^7+z^5+z^4+z^3+z^2+z", "z^7+z^6+z^3+z^2", "z^7+z^5+z^4+z^2+1"),
        ];
        for pair in polynomials_vector {
            let (a_str, b_str, r_str): (&str, &str, &str) = pair;
            let a = field.from_string(a_str);
            let b = field.from_string(b_str);
            let r = field.from_string(r_str);
            // println!("{} == {}", (a*b).to_string(), r.to_string());
            assert!(a*b == r);
        }
    }

    #[test]
    fn test_multiplicative_inverse(){
        let field = BinaryExtensionModuloConstructor::new("z^8+z^4+z^3+z+1", 'z');
        let polynomials_vector: Vec<(&str, &str)> = vec![
            ("z^7+z^3+z^2", "z^7+z^6+z^5+z^4+z^2+z+1"),
            ("z^7+z^6+z^4+z^2+z+1", "z^7+z^6+z^5+z^3+z"),
            ("z^7+z^6+z^5+z^4+z^2", "z^6+z^5+z^3"),
            ("z^7+z^6+z^2+z+1", "z^3+z^2+z+1"),
            ("z^7+z^6+z^5+z^3+1", "z^6+z^3+z^2+z"),
        ];
        for pair in polynomials_vector {
            let (a_str, r_str): (&str, &str) = pair;
            let a = field.from_string(a_str);
            let r = field.from_string(r_str);
            let inverse = !a;  // a^-1;
            // println!("({})^-1 = {} == {}", a_str, inverse.to_string(), r.to_string());
            assert!(inverse == r);
        }
        // TODO: test a ring. In a ring not all the elements have inverse so both cases must be tested.
    }

    #[test]
    fn test_shift(){
        let field = BinaryExtensionModuloConstructor::new("z^8+z^4+z^3+z+1", 'z');
        let polynomials_vector: Vec<(&str, u8, &str, &str)> = vec![
            ("z^4", 1, "z^5", "z^3"),
        ];
        for pair in polynomials_vector {
            let (a_str, i, l_str, r_str): (&str, u8, &str, &str) = pair;
            let a = field.from_string(a_str);
            let a_lshifted = field.from_string(a_str) << i;
            let a_rshifted = field.from_string(a_str) >> i;
            let l = field.from_string(l_str);
            let r = field.from_string(r_str);
            // println!("{} << {} = {} == {}", a_str.to_string(), i, a_lshifted.to_string(), l.to_string());
            // println!("{} >> {} = {} == {}", a_str.to_string(), i, a_rshifted.to_string(), r.to_string());
            assert!(a_lshifted == l);
            assert!(a_rshifted == r);
        }
    }
    #[test]
    fn test_cyclic_shift(){
        let field = BinaryExtensionModuloConstructor::new("z^8+z^4+z^3+z+1", 'z');
        let polynomials_vector: Vec<(&str, u8, &str, &str)> = vec![
            ("z^7", 0, "z^7", "z^7"),
            ("z^7", 1, "1", "z^6"),
            ("z^7", 2, "z^1", "z^5"),
            ("z^7", 3, "z^2", "z^4"),
            ("z^7", 4, "z^3", "z^3"),
            ("z^7", 5, "z^4", "z^2"),
            ("z^7", 6, "z^5", "z"),
            ("z^7", 7, "z^6", "1"),
            ("z^7", 8, "z^7", "z^7"),
            ("z^4", 1, "z^5", "z^3"),
        ];
        for pair in polynomials_vector {
            let (a_str, i, l_str, r_str): (&str, u8, &str, &str) = pair;
            let a = field.from_string(a_str);
            let a_lshifted = field.from_string(a_str).cyclic_shl(i);
            let a_rshifted = field.from_string(a_str).cyclic_shr(i);
            let l = field.from_string(l_str);
            let r = field.from_string(r_str);
            println!("{} << {} = {} == {}", a_str.to_string(), i, a_lshifted.to_string(), l.to_string());
            println!("{} >> {} = {} == {}", a_str.to_string(), i, a_rshifted.to_string(), r.to_string());
            assert!(a_lshifted == l);
            assert!(a_rshifted == r);
        }
    }
}