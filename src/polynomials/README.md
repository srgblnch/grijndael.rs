# Polynomials, the maths behind the Rijndael

This appendix complements the generalization with a short review of the maths 
behind the _Rijndael_ algorithm. What distinguishes this algorithm, especially 
from its predecessor DES, is the fact that everything relies on public 
knowledge and mathematical elegance. 

[[_TOC_]]

## Algebra review

The fundamental building block of the _Rijndael_ is the _binary field_ 
$`\mathbb{F}_{2}`$ and its properties are propagated to the field extension 
$`\mathbb{F}_{2^w}`$ which in the original _Rijndael_ $`w=8`$ computationally 
corresponded to _Byte_ operations. There is another structure built above 
that _extends the extension_ up to the use of 32 bits $`\mathbb{F}_{2^{w^l}}`$
(which $`l=4`$ in the original _Rijndael_) in the 
[$`\texttt{mixcolumns()}`$](src/mixcolumns.md).

There are two depictions used for those mathematical structures: vectors and 
polynomials. Each of them may help in certain operations or may complicate them 
compared with the other. Then, depending on the need, one or the other will be 
used.

Because two extensions of the binary field co-exist, certain notation licenses 
have been taken, starting with using two _indefinite variables_ in the 
polynomial notation. In the "single" binary field extension 
$`\mathbb{F}_{2^w}`$, the _indefinite variable_ is tagged as $`z`$. In the 
extension, $`\mathbb{F}_{2^{w^{l}}}`$, the coefficients can be written with 
$`\texttt{hexadecimal}`$ notation (as vectors) or polynomial with $`z`$, and the 
_indefinite variable_ is tagged as $`x`$.

## About the binary field extensions

Used almost everywhere in _Rijndael_, the binary field is $`w`$-th extended as 
$`\mathbb{F}_{2^w}`$. Using a vector representation, one sees it close to the 
memory representation of a bit stream, but as a polynomial, the operation is 
reduced by a $`w`$ degree modulo. This module can be _reducible_ or not, 
producing a representation that has an algebraic _commutative ring_ 
$`(R, \oplus, \otimes)`$ structure or a _field_ $`(K, \oplus, \otimes)`$, 
denoting it as:
```math
\mathbb{F}_{2^w}=\frac{\mathbb{F}_{2}[z]}{m(z)}
```
The left side of the equality denotes a vectorized view of the structure, and 
the right side shows a polynomial view.

Implementations must always have _side channel attacks_ very present. While 
working with the binary field extension, the product operation can be an 
example of code that changes from one notation to another based on what is 
better. As one can see in the [$`\textcolor{red}{algorithm}`$](alg:polynomialProduct), the 
polynomial operand polynomial representations have been changed to vectorial 
to operate the _bit-stream_ and protect the implementation by doing the same 
operations (bitwise shifts $`<<`$ and bitwise xors $`\oplus`$) independently 
of the input and only having differences base on the input in the memory 
movements.

<table>
  <tr>
    <td> <b>Algorithm</b>: </td>
    <td> Side channel protected product </td>
  </tr><tr>
    <td> <b>Require</b>: </td>
    <td> Polynomials $a(z)$ and $b(z) \in \frac{\mathbb{F}_{2}[z]}{m(z)}$ </td> 
  </tr><tr>
    <td> <b>Ensure</b>: </td>
    <td> $c(z)=a(z)\otimes b(z)$ </td>
  </tr>
  <tr>
    <td colspan=2>
      $c \leftarrow 0$ <br> 
      <b>for</b> $i=0$ <b>to</b> $i=gr(b(z))$ <br>
      &nbsp;&nbsp;&nbsp;&nbsp; $c' \leftarrow c \oplus (a \ll i)$ <br>
      &nbsp;&nbsp;&nbsp;&nbsp; <b>if</b> $b$ & $(1 \ll i) \neq 0$ <b>then</b> <br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $c \leftarrow c'$ <br>
      <b>return</b> $c$ 
    </td>
  </tr>
</table>

<!--
```math
\begin{algorithm}
\caption{Side channel protected product}
\label{alg:polynomialProduct}
\begin{algorithmic}[1]
\REQUIRE Polynomials $a(z)$ and $b(z) \in \frac{\mathbb{F}_{2}[z]}{m(z)}$
\ENSURE $c(z)=a(z)\otimes b(z)$
\STATE c $\leftarrow$ 0
\FOR{i = 0 \TO i=gr($b(z)$)}
\STATE aShifted $\leftarrow$ a $<<$ i
\STATE temp $\leftarrow$ c $\oplus$ aShifted
\IF{b \& (1 $<<$ i) $\neq$ 0}
\STATE c $\leftarrow$ temp
\ENDIF
\ENDFOR
\end{algorithmic}
\end{algorithm}
```
-->

Another peculiarity that should be mentioned is the inversion when one sees 
the $`w`$-th binary field extension as a polynomial modulo $`m(z)`$. Under this 
representation, not all the elements have a _multiplicative inverse_. Then it 
is necessary to use an algorithm to evaluate candidates. This is not used when 
the implementation is used for cipher and deciphering.

To know if an element of a ring is invertible, it is enough to check if the 
_Greatest Common Divisor_ ($`\texttt{gcd}`$) between the elements and the
modulo polynomial corresponds to $`1(z)`$ (neutral element of the product 
operation). Or, in other words, if the element is not a divisor of the modulo. 
To get this $`\texttt{gcd}`$ and also have the inverse as a calculation result, 
the _Extended Euclidean Greatest Common Divisor_ can be used. We previously did 
not use algorithms from the literature because they are optimized for larger 
extension fields. Still, in this case, we can use Algorithm 2.47 described in 
\cite{DAS_guide} and transcribed in Algorithm \ref{alg:eGCD}.

## About the ring where its coefficients are elements of an underlying binary field extension

TODO
```math
(\mathbb{F}_{2^w})^l=\mathbb{F}_{2^{w^{l}}}=\mathbb{F}_{2^{w\times l}}=\frac{\mathbb{F}_{2^w}[x]}{l(x)}
```