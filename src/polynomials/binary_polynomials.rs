use std::cmp::{PartialEq};
use std::ops::{Add,AddAssign,Neg,Sub,SubAssign,
               Mul,MulAssign,BitXor,Not,
               Shl,ShlAssign,Shr,ShrAssign};

#[derive(Clone)]
struct BinaryPolynomial {
    value: u16,
    variable: char,
}

#[derive(Clone)]
pub struct BinaryExtensionModuloConstructor {
    modulo: BinaryPolynomial,
}

pub struct BinaryExtensionModulo {
    polynomial: BinaryPolynomial,
    modulo: BinaryExtensionModuloConstructor,
}

// constant time degree
fn degree(coefficients: u16) -> u8 {
    let mut mask: u16 = std::u16::MAX;
    let mut degree: u8 = 15;
    while mask > 0 {
        if (coefficients & mask) == 0 && degree > 0 {
            degree -= 1;
        }
        mask <<= 1;
    }
    degree
}


impl BinaryPolynomial {
    pub fn new(polynomial: &str, variable: char) -> BinaryPolynomial {
        BinaryPolynomial { value: Self::interpret_from_str(polynomial, variable), variable }
    }
    pub fn from_coefficients(coefficients: u16, variable: char) -> BinaryPolynomial {
        BinaryPolynomial { value: coefficients, variable }
    }
    pub fn variable(&self) -> char {
        self.variable
    }
    pub fn coefficients(&self) -> u16 {
        self.value
    }
    pub fn degree(&self) -> u8 {
        degree(self.coefficients())
    }
    pub fn hamming_weight(&self) -> u8 {
        // TODO: transform to constant time bit iteration, but avoid string transformation
        if self.value == 0 {
            0
        } else {
            format!("{:b}", self.value).matches('1').count() as u8
        }
    }
    pub fn to_string(&self) -> String {
        if self.value == 0 {
            String::from("0")
        } else {
            let mut terms: String = String::new();
            let bitstream: String = format!("{:b}", self.value);
            let mut exponent: i8 = bitstream.len() as i8 - 1;
            for coefficient in bitstream.chars() {
                if coefficient == '0' {
                    // nothing to append
                } else if exponent == 0 {  // and coefficient '1'
                    terms.push_str("+1");
                } else if exponent == 1 {  // and coefficient '1'
                    // append self.variable, 'z^1' but the equivalent 'z'
                    terms = terms + format!("+{}", self.variable).as_str();
                } else {
                    // append self.variable^exponent
                    terms = terms + format!("+{}^{}", self.variable, exponent).as_str();
                }
                exponent -= 1;
            }
            terms[1..].to_string()  // remove the last '+'
        }
    }
    fn interpret_from_str(polynomial_str: &str, variable: char) -> u16 {
        let mut value: u16 = 0;
        for term in polynomial_str.split('+') {
            if term.len() > 2 {  // z^i
                if term.chars().nth(0) == Some(variable) && term.chars().nth(1) == Some('^') {
                    let exponent: u8 = term[2..].parse::<u8>().unwrap();
                    value |= 1 << exponent;
                } else {
                    panic!("Cannot interpret {} in {}", term, polynomial_str)
                }
            } else if term.len() == 1 {
                if term.chars().nth(0) == Some(variable) {  // z == z^1
                    value |= 1 << 1
                } else if term.chars().nth(0) == Some('1') {  // 1 == z^0
                    value |= 1
                } else if term.chars().nth(0) == Some('0') {
                    // nothing to do
                } else {
                    panic!("Cannot interpret {} in {}", term, polynomial_str)
                }
            } else {
                panic!("Cannot interpret {} in {}", term, polynomial_str)
            }
        }
        value
    }
}

impl BinaryExtensionModuloConstructor {
    pub fn new(modulo: &str, variable: char) -> BinaryExtensionModuloConstructor {
        let modulo_obj: BinaryPolynomial = BinaryPolynomial::new(modulo, variable);
        BinaryExtensionModuloConstructor { modulo: modulo_obj }
    }
    pub fn from_string(&self, polynomial: &str) -> BinaryExtensionModulo {
        let polynomial_obj: BinaryPolynomial = BinaryPolynomial::new(polynomial, self.variable());
        BinaryExtensionModulo::new(polynomial_obj, self.clone())
    }
    pub fn from_coefficients(&self, coefficients: u16) -> BinaryExtensionModulo {
        let polynomial_obj: BinaryPolynomial = BinaryPolynomial::from_coefficients(coefficients, self.modulo.variable());
        BinaryExtensionModulo::new(polynomial_obj, self.clone())
    }
    pub fn variable(&self) -> char {
        self.modulo.variable
    }
    pub fn coefficients(&self) -> u16 {
        self.modulo.coefficients()
    }
    pub fn degree(&self) -> u8 {
        self.modulo.degree()
    }
    pub fn to_string(&self) -> String {
        self.modulo.to_string()
    }
}


impl BinaryExtensionModulo {
    // TODO: operations
    //  - egcd, inv, {,cyclic}{l,r}shift
    fn new(polynomial: BinaryPolynomial, modulo: BinaryExtensionModuloConstructor) -> BinaryExtensionModulo {
        if polynomial.value >= modulo.coefficients() {
            let remainder: u16 = BinaryExtensionModulo::division(polynomial.value, modulo.coefficients())[1];
            let reduced_polynomial: BinaryPolynomial = BinaryPolynomial::from_coefficients(remainder, modulo.variable());
            BinaryExtensionModulo { polynomial: reduced_polynomial, modulo }
        } else {
            BinaryExtensionModulo { polynomial, modulo }
        }
    }
    pub fn variable(&self) -> char {
        self.modulo.variable()
    }
    pub fn coefficients(&self) -> u16 {
        self.polynomial.coefficients()
    }
    pub fn degree(&self) -> u8 {
        self.polynomial.degree()
    }
    pub fn modulodegree(&self) -> u8 {
        self.modulo.degree()
    }
    pub fn hamming_weight(&self) -> u8 {
        self.polynomial.hamming_weight()
    }
    pub fn is_zero(&self) -> bool {
        self.polynomial.value == 0
    }
    pub fn is_one(&self) -> bool {
        self.polynomial.value == 1
    }
    // pub fn is_invertible()
    pub fn to_string(&self) -> String {
        format!("{}", self.polynomial.to_string())
    }
    pub fn to_string_modulo(&self) -> String {
        format!("{} (mod {})", self.polynomial.to_string(), self.modulo.to_string())
    }
    pub fn to_hexadecimal(&self) -> String {
        format!("0x{:X}", self.coefficients())
    }
    fn multiplication(&self, multiplicant: u16, multiplier: u16) -> u16 {
        let mut result: u16 = 0;
        let mut mask: u16 = 1;
        let mut step: u16 = 0;
        // println!("<multiplication multiplicant={:b} divisor={:b}>", multiplicant, multiplier);
        while mask < self.modulo.coefficients() {
            let bit: bool = (multiplier & mask) != 0;
            result = self.multiplication_step(multiplicant, bit, step, result);
            // println!("step {} accum {:b}", step, result);
            mask <<= 1;
            step += 1;
        }
        // println!("result {:b}", result);
        // println!("<multiplication>");
        result
    }
    fn multiplication_step(&self, multiplicant: u16, bit: bool, shift: u16, accum: u16) -> u16{
        let result: u16 = accum ^ (multiplicant << shift);
        if bit {
            result
        } else {
            accum
        }
    }
    fn xtimes(&self) -> Self {
        self.modulo.from_coefficients(self.coefficients() << 1)
    }
    pub fn division(divident: u16, divisor: u16) -> [u16;2] {
        if divisor == 0 {
            panic!("division by zero")
        }
        // println!("<division divident={:b} divisor={:b}>", divident, divisor);
        let divident_degre: u8 = degree(divident);
        let divisor_degre: u8 = degree(divisor);
        let mut quotient: u16 = 0;
        let mut rest: u16 = divident;
        // println!("degree(divident) = degree({:b}) = {}", divident, divident_degre);
        // println!("degree(divisor) = degree({:b}) = {}", divisor, divisor_degre);
        let mut divident_index: i8 = divident_degre as i8;
        let mut rest_degree: u8 = divident_degre;
        let mut degree_difference: i8 = divident_index-divisor_degre as i8;
        while rest_degree >= divisor_degre && degree_difference >= 0 {
            // println!("WHILE degree(rest) >= degree(divisor) && gr_diff >= 0 : {} >= {} && {} >= 0", rest_degree, divisor_degre, degree_difference);
            let mut take: u16 = 0;
            if degree_difference >= 0 {
                let mask:u16 = std::u16::MAX << degree_difference;
                take = rest & mask;
                // println!("\ttake = {:b} = {:b} & {:b} = rest & mask", take, rest, mask);
            } else {
                take = rest;
                // println!("\ttake = {:b} = rest", take);
            }
            let substractor: u16 = divisor << degree_difference;
            // println!("\tsubstractor = {:b} = {:b} << {} = divisor << degree_difference", substractor, divisor, degree_difference);
            if degree(take) == degree(substractor) {
                let difference: u16 = take ^ substractor;
                if degree_difference > 0 {
                    let new_quotient = quotient | (1<< degree_difference);
                    // println!("\tquotient = {:b} = {:b} | (1 << {}) = quotient | (1 << gr_diff)", new_quotient, quotient, degree_difference);
                    quotient = new_quotient;
                    let mask: u16 = (1 << degree_difference) - 1;
                    rest = difference | (divident & mask);
                    // println!("\trest = {:b} = {:b} | ({:b} & {:b})", rest, difference, divident, mask);
                } else {
                    let new_quotient = quotient | 1;
                    // println!("\tquotient = {:b} = {:b} | 1 = quotient | 1", new_quotient, quotient);
                    quotient = new_quotient;
                    rest = difference;
                }
                rest_degree = degree(rest);
            }
            divident_index -= 1;
            degree_difference = divident_index - divisor_degre as i8;
        }
        // println!("quotient = {:b}", quotient);
        // println!("rest = {:b}", rest);
        // println!("</division>");
        // println!("divident / divisor = {:b} / {:b} = ({:b},{:b}) = (quotient,rest)", divident, divisor, quotient, rest);
        [quotient, rest]
    }
    fn egcd(a: u16, b: u16) -> [u16;3] {
        let (mut u, mut v): (u16, u16 ) = (a, b);
        // println!("u = {}, v = {}", u, v);
        let (mut g1, mut g2, mut h1, mut h2): (u16, u16, u16, u16) = (1, 0, 0, 1);
        // println!("g1 = {}, g2 = {}, h1 = {}, h2 = {}", g1, g2, h1, h2);
        while u != 0 {
            let mut j: i16 = degree(u) as i16 - degree(v) as i16;
            if j < 0 {
                // println!("{} < 0", j);
                // u <-> v
                (u, v) = (v, u);  // swap(&u, &v);
                // g1 <-> g2
                (g1, g2) = (g2, g1);
                // h1 <-> h2
                (h1, h2) = (h2, h1);
                j = -j;
            }
            u = u ^ (v << j);
            g1 = g1 ^ (g2 << j);
            h1 = h1 ^ (h2 << j);
            // println!("u = {}, g1 = {}, h1 = {}", u, g1, h1);
        }
        let d: u16 = v;
        let g: u16 = g2;
        let h: u16 = h2;
        // println!("d = {}, g = {}, h = {}", d, g, h);
        return [d, g, h];
    }
    fn gcd(a: u16, b: u16) -> u16 {
        BinaryExtensionModulo::egcd(a, b)[0]
    }
    fn multiplicative_inverse(self) -> Self {
        if self.coefficients() == 0 {
            return self;
        }
        let egcd: [u16;3] = BinaryExtensionModulo::egcd(self.coefficients(), self.modulo.coefficients());
        let gcd: u16 = egcd[0];
        let multiplicative_inverse: u16 = egcd[1];
        if gcd != 1 {
            panic!("The inverse of {} modulo {} doens't exist!", self.polynomial.to_string(), self.modulo.to_string());
        }
        // println!("gcd({}) = {}", self.polynomial.to_string(), gcd);
        return self.modulo.from_coefficients(multiplicative_inverse);
    }
    fn split_coefficients(&self, cut_point: u8) -> [u16;2] {
        // println!("<SplitCoefficients coefficients={:08b} cut_point={}>", self.coefficients(), cut_point);
        let modulo_degree: u8 = self.modulo.degree();
        // println!("modulo's degree: {}", modulo_degree);
        if cut_point > modulo_degree {
            panic!("Split out of range");
        }
        let mask_lsb: u16 = (1 << cut_point) - 1;
        let mask_msb: u16 = ((1 << modulo_degree) - 1) ^ mask_lsb;
        // println!("mask: {:08b} | {:08b}", mask_msb, mask_lsb);
        let lsb: u16 = self.coefficients() & mask_lsb;
        let msb: u16 = self.coefficients() & mask_msb;
        // println!("split coefficients {:b}_{:b}", msb, lsb);
        // println!("</SplitCoefficients>");
        return [msb, lsb];
    }
    pub fn cyclic_shl(self, i: u8) -> Self {
        // println!("</CyclicShiftLeft polynomial={} lshift={}>", self.polynomial.to_string(), i);
        let shift: u8 = self.modulo.degree()-(i%self.modulo.degree());
        let split: [u16;2] = self.split_coefficients(shift);
        if shift == self.modulo.degree() {
            return self;
        }
        let msb: u16 = split[0] >> shift;
        let lsb: u16 = split[1] << self.modulo.degree()-shift;
        // println!("cyclic shifted: {:b}_{:b}", msb, lsb);
        // println!("</CyclicShiftLeft>");
        return self.modulo.from_coefficients(msb|lsb);
    }
    pub fn cyclic_shr(self, i: u8) -> Self {
        // println!("</CyclicShiftRight polynomial={} rshift={}>", self.polynomial.to_string(), i);
        let shift: u8 = i%self.modulo.degree();
        let split: [u16;2] = self.split_coefficients(shift);
        if shift == 0 {
            return self;
        }
        let msb: u16 = split[0] >> shift;
        let lsb: u16 = split[1] << shift-1;
        // println!("cyclic shifted: {:b}_{:b}", msb, lsb);
        // println!("</CyclicShiftRight>");
        return self.modulo.from_coefficients(msb|lsb);
    }
}

impl PartialEq for BinaryExtensionModulo {
    fn eq(&self, other: &Self) -> bool {
        self.modulo.coefficients() == other.modulo.coefficients() &&
            self.coefficients() == other.coefficients()
    }
    fn ne(&self, other: &Self) -> bool {
        ! (self == other)
    }
}

impl Add for BinaryExtensionModulo {
    type Output = Self;

    fn add(self, other: Self) -> Self::Output {
        self.modulo.from_coefficients(self.coefficients() ^ other.coefficients())
    }
}

impl AddAssign for BinaryExtensionModulo {
    fn add_assign(&mut self, other: Self) {
        *self = self.modulo.from_coefficients(self.coefficients() ^ other.coefficients());
    }
}

impl Neg for BinaryExtensionModulo {
    type Output = Self;

    fn neg(self) -> Self::Output {
        self
    }
}

impl Sub for BinaryExtensionModulo {
    type Output = Self;

    fn sub(self, other: Self) -> Self::Output {
        self.modulo.from_coefficients(self.coefficients() ^ other.coefficients())
    }
}

impl SubAssign for BinaryExtensionModulo {
    fn sub_assign(&mut self, other: Self) {
        *self = self.modulo.from_coefficients(self.coefficients() ^ other.coefficients());
    }
}

impl Mul for BinaryExtensionModulo {
    type Output = Self;

    fn mul(self, other: Self) -> Self::Output {
        self.modulo.from_coefficients(self.multiplication(self.coefficients(), other.coefficients()))
    }
}

impl MulAssign for BinaryExtensionModulo {
    fn mul_assign(&mut self, other: Self) {
        *self = self.modulo.from_coefficients(self.multiplication(self.coefficients(), other.coefficients()));
    }
}

// Use a^-1 or !a notation to Invert a polynomial
// impl BitXor for BinaryExtensionModulo {
impl Not for BinaryExtensionModulo {
    type Output = Self;

    //fn bitxor(self, exp: i8) -> Self::Output {
    //     if exp != -1 {
    //         panic!("Inversion notation is to power to minus 1");
    //     }
    fn not(self) -> Self::Output {
        self.multiplicative_inverse()
    }
}

impl Shl<u8> for BinaryExtensionModulo {
    type Output = Self;

    fn shl(self, i: u8) -> Self::Output {
        self.modulo.from_coefficients(self.coefficients() << i)
    }
}

impl ShlAssign<u8> for BinaryExtensionModulo {
    fn shl_assign(&mut self, i: u8) {
        *self = self.modulo.from_coefficients(self.coefficients() << i)
    }
}

impl Shr<u8> for BinaryExtensionModulo {
    type Output = Self;

    fn shr(self, i: u8) -> Self::Output {
        self.modulo.from_coefficients(self.coefficients() >> i)
    }
}

impl ShrAssign<u8> for BinaryExtensionModulo {
    fn shr_assign(&mut self, i: u8) {
        *self = self.modulo.from_coefficients(self.coefficients() >> i)
    }
}
