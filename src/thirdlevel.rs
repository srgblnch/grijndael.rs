pub struct Word {
    r: u8,
    ws: u8,
}

impl Word {
    pub fn new(n_rows: u8, word_size: u8) -> Self {
        Word { r:n_rows, ws: word_size }
    }
    // pub fn to_list(superword: str) -> Vec {
    //
    // }
    // pub fn from_list(words: Vec) -> str {
    //
    // }
}

pub struct Long {
    ws: u8,
}

impl Long {
    pub fn new(word_size: u8) -> Self {
        Long {ws: word_size }
    }
    // pub fn to_array(...) -> Vec {
    //
    // }
    // pub fn from_array(...) -> ... {
    //
    // }
}