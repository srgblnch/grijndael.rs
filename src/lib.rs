use std::cmp;

mod addroundkey;
mod keyexpansion;
mod mixcolumns;
mod shiftrows;
mod subbytes;

use addroundkey::AddRoundKey;
use keyexpansion::KeyExpansion;
use mixcolumns::MixColumns;
use shiftrows::ShiftRows;
use subbytes::SubBytes;

pub struct Grijndael {
    n: u8,
    r: u8,
    c: u8,
    ws: u8,
    kc: u8,
    keyexpansion: KeyExpansion,
    subbytes: SubBytes,
    shiftrows: ShiftRows,
    mixcolumns: MixColumns,
    addroundkey: AddRoundKey,
}

impl Grijndael {
    pub fn new(n_rounds: u8, n_rows: u8, n_columns: u8, word_size:u8, n_key_columns: u8) -> Self {
        let min_rounds: u8 = cmp::max(n_columns, n_key_columns) + 6;
        if n_rounds < min_rounds {
            println!("Perhaps there are not enough rounds: max(N_k,N_c)+6 = max({},{})+6 = {}",
                n_columns, n_key_columns, min_rounds)
        }
        let grijndael = Grijndael {
            n: n_rounds, r: n_rows, c: n_columns, ws: word_size, kc: n_key_columns,
            keyexpansion: KeyExpansion::new(n_rounds, n_rows, n_columns, word_size, n_key_columns),
            subbytes: SubBytes::new(),
            shiftrows: ShiftRows::new(), mixcolumns: MixColumns::new(),
            addroundkey: AddRoundKey::new()
        };
        println!("Initialising gRijndael ({},{},{},{},{}): block={}bits key={}bits",
            grijndael.n_rounds(), grijndael.n_rows(), grijndael.n_columns(), grijndael.word_size(),
                 grijndael.n_key_columns(), grijndael.block_size(), grijndael.key_size());
        grijndael
    }
    pub fn n_rounds(&self) -> u8 { self.n }
    pub fn n_rows(&self) -> u8 { self.r }
    pub fn n_columns(&self) -> u8 { self.c }
    pub fn word_size(&self) -> u8 { self.ws }
    pub fn n_key_columns(&self) -> u8 { self.kc }
    pub fn block_size(&self) -> u16 {  self.r as u16 * self.c as u16 * self.ws as u16 }
    pub fn key_size(&self) -> u16 { self.r as u16 * self.kc as u16 * self.ws as u16 }
    // pub fn cipher()
    // pub fn decipher()
}
