#[path = "sbox.rs"] mod sbox;
#[path = "thirdlevel.rs"] mod thirdlevel;

use sbox::SBox;
use thirdlevel::{Word,Long};

pub struct KeyExpansion {
    n: u8,
    r: u8,
    c: u8,
    ws: u8,
    kc: u8,
    sbox: SBox,
    word: Word,
}

impl KeyExpansion {
    pub fn new(n_rounds: u8, n_rows: u8, n_columns: u8, word_size: u8, n_key_columns: u8) -> Self {
        let expander = KeyExpansion {
            n: n_rounds, r: n_rows, c: n_columns, ws: word_size, kc: n_key_columns,
            sbox: SBox::new(word_size), word: Word::new(n_rows, word_size)
        };
        // expander.initialize(key: Long::new(word_size));
        expander
    }
    // fn initialize(key: Long) {
    //
    // }
}